---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie** |  |
|---|---|
| Release | 1.0 RC-test|
| Versie | 1.0 versie 0.5 concept, ter informatie Ketenraad KIK-V. |
| Doel | Release 0.5 betreft de functioneel omschreven behoefte van cliënten aan keuzeinformatie over een verpleeghuiszorg(locatie), vertaald in een uitvraag van ketenpartijen om hen daarbij te helpen. |
| Doelgroep | Zorgkantoren; Zorgaanbieders verpleeghuiszorg; Patiëntenfederatie Nederland; Actiz; Zorgaanbieders verpleeghuiszorg. |
| Totstandkoming | Release 0.5 is, ondersteund door het programma KIK-V, met een vertegenwoordiging van zorgkantoren, Patiëntenfederatie Nederland en ActiZ ontwikkeld. |
| Inwerkingtreding | Release 1.0 wordt in 2023 vastgesteld door de Ketenraad KIK-V op basis van versie 0.9, welke begin 2023 wordt vastgesteld. |
| Operationeel toepassingsgebied | Cliëntkeuzeinformatie |
| Status | Ter informatie Ketenraad KIK-V. |
| Functionele scope | Release 1.0 omvat de vragen die voor cliënten nodig zijn om een keuze voor een zorglocatie te maken |
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0). |
| Changelog | Een overzicht van de veranderingen per wijziging (lees: commit) zijn weergegeven in een [changelog].(/Documentatie/release-info/Changelog.md) |
