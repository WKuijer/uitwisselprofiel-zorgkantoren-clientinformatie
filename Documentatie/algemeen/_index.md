---
title: Algemeen
weight: 2
---
# Algemeen

## Doel van de uitvraag
Het uitwisselprofiel Ondersteuning cliëntkeuze verpleging en verzorging bevat keuze informatie voor kwetsbare ouderen, mantelzorgers en naasten ter ondersteuning van de keuze voor een passende locatie voor intramurale zorg en geclusterd volledig pakket thuis (VPT).
