---
title: Semantische interoperabiliteit
weight: 3
---
## Introductie
De gegevensset die nodig is om cliënten te begeleiden in het maken van de juiste keuze voor een zorglocatie is opgebouwd rondom de volgende thema’s (o.b.v. verschillende onderzoeken onder cliënten, mantelzorgers en naasten uitgevoerd door Patiëntenfederatie Nederland): Algemeen - Doelgroep - Personeel - Locatie - Culturele identiteit/ geloofsovertuiging - Wachttijden - Communicatie - Overige thema’s.

## Algemene vragen
| **0.1 Wat is de naam van de organisatie?**                                                                                                                    |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **0.2 Wat is het KvK-nummer van de organisatie?**                                                                                                             |
| **0.3 Wat is de AGB code van de organisatie?**                                                                                                                |
| **0.4 Wat is het NZa nummer van de organisatie?**                                                                                                             |
| **0.5 Wat is de postcode van de organisatie?**                                                                                                                |
| **0.6 Wat is het huisnummer van de organisatie?**                                                                                                             |
| **0.7 Wat is de naam van de vestiging?**                                                                                                                      |
| **0.8 Wat is het KvK-vestigingsnummer van de vestiging?**                                                                                                     |
| **0.9 Wat is de AGB code van de vestiging**                                                                                                               |
| **0.10 Wat is de postcode van de vestiging?**                                                                                                                 |
| **0.11 Wat is het huisnummer van de vestiging?**                                                                                                              |
| Deze gegevens zijn nodig om de juiste organisatie te identificeren, de juiste doelgroep te selecteren en om eventueel vestigingsgegevens te kunnen koppelen.  |

**Aggregatieniveau**

Het aggregatieniveau is voor alle indicatoren organisatie- en vestigingsniveau.

**Cliëntformulering van de vraag en bijpassende “business” indicator**

Het uitwisselprofiel is opgesteld vanuit cliëntperspectief: welke informatiebehoefte heeft een cliënt, mantelzorgers en/of zijn of haar naasten om een goede keuze voor een zorglocatie te kunnen maken? Deze behoeften zijn gebruikt om een beste passende keuze te maken voor een indicator die vanuit de bronsystemen van zorgaanbieders kan worden gegenereerd. De uiteindelijke vraag en antwoord die worden uitgewisseld kennen dus een andere formulering dan de cliëntvraag.

**Wel of geen onderdeel van de Modelgegevensset?**

De hoge mate van actualiteit en de aard van de vragen maakt dat niet alle vragen met de gegevenselementen uit de Modelgegevensset beantwoord kunnen worden. Per informatievraag/ indicator is aangegeven of dit het geval is of niet. Indien een vraag nu niet in de Modelgegevensset past, gaat uitgezocht worden wat de meest effectieve en efficiënte manier van registreren (en vervolgens uitwisselen) voor betrokken partijen is. Indien blijkt dat dit niet mogelijk is, kan het alternatief ook nog zijn om de vraag geheel te laten vervallen.

## Thema: Doelgroep
|                                                                                                                                                                                                                                                                                                                                                                                   |                                                                   |                                     |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------|-------------------------------------|
| **1.1 Levert deze locatie de zorg die in mijn indicatie staat?<br>**                                                                                                                                                                                                                                                                                                              |                                                                   |                                     |
| **Doel en achtergrond**                                                                                                                                                                                                                                                                                                                                                           | **Indicator**                                                     | **Onderdeel van Modelgegevensset?** |
| De zorgvrager, mantelzorger of naaste wil weten of de zorgvrager met de indicatie terecht kan bij de vestiging.                                                                                                                                                                                                                                                                   | Soort Wlz-indicaties  per vestiging                               | Ja                                  |
| **1.2.1 Welke leveringsvorm(en) biedt deze locatie?**                                                                                                                                                                                                                                                                                                                             |                                                                   |                                     |
| Welke leveringsvormen worden er op deze vestiging geleverd?                                                                                                                                                                                                                                                                                                                       | Soort leveringsvormen per vestiging                               | Ja                                  |
| **1.2.2 Welke soorten zorg levert deze locatie?**                                                                                                                                                                                                                                                                                                                                 |                                                                   |                                     |
| Soorten zorg vanuit zorgprestatielijst 055 Wlz (bijvoorbeeld logeergelegenheid voor tijdelijk zorgvragen)                                                                                                                                                                                                                                                                         | Soort zorgprestaties per vestiging                                | Ja                                  |
| **1.2.2 Is een tijdelijke opname mogelijk op deze locatie?**                                                                                                                                                                                                                                                                                                                      |                                                                   |                                     |
| Kan een zorgvrager tijdelijk worden opgenomen, bijvoorbeeld na een ziekenhuisopname?<br>Deze vraag lijkt wat buiten de scope geformuleerd. Op dit moment zijn er geen onderscheidende prestatie voor tijdelijk verblijf in de Wlz. Indien een vestiging ELV-zorg levert, geeft dit  een indicatie dat deze vestiging tijdelijk verblijf aanbiedt.                                 | ELV-mogelijkheid per vestiging                                    | Ja                                  |
| **1.3.1 Als ik moet betalen voor wonen: kan ik tot de sociale huurgrens terecht?**                                                                                                                                                                                                                                                                                                |                                                                   |                                     |
|                                                                                                                                                                                                                                                                                                                                                                                   | Ja/ Nee                                                           | Ja                                  |
| **1.3.2 FACULTATIEF Wat is de minimale huurprijs, de maximale huurprijs en de servicekosten?**                                                                                                                                                                                                                                                                                    |                                                                   |                                     |
| De zorgvrager wil weten hoeveel hij/zij moet betalen om op een vestiging te kunnen wonen.                                                                                                                                                                                                                                                                                         | Minimale-maximale huurprijs en servicekosten per soort woonruimte |                                     |
| **1.4 Waarin is deze locatie gespecialiseerd?**                                                                                                                                                                                                                                                                                                                                   |                                                                   |                                     |
| Als de zorgvrager een bepaalde aandoening heeft (bijv. ALS, Korsakov, Huntington, NAH): heeft de vestiging daar dan genoeg kennis en ervaring mee?                                                                                                                                                                                                                                | Specialisaties per vestiging                                      | Ja                                  |
| **1.5 Kan ik op deze locatie terecht met een rechterlijke machtiging, IBS of een Wzd/artikel 21 verklaring**                                                                                                                                                                                                                                                                    |                                                                   |                                     |
| Kun je als zorgvrager op de locatie (blijven) wonen als hij/zij onder de Wzd valt (psychogeriatrische aandoening, CIZ indicatie met als grondslag psychogeriatrische aandoening of een gelijkgestelde aandoening als Korsakov, Huntington of NAH, indien er sprake is van regieverlies vergelijkbaar met een psychogeriatrische aandoening en dit kan leiden tot ernstig nadeel)? | Wel/ geen Wzd-locatie aangemerkt in het locatieregister           | Ja                                  |

## Thema: Personeel

|                                                                                                                                                                                               |                                                                                                                                                                          |                                                                                                                |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
| **2.1 Zie ik zoveel mogelijk vaste gezichten?<br>**                                                                                                                                           |                                                                                                                                                                          |                                                                                                                |
| **Doel en achtergrond**                                                                                                                                                                       | **Indicator**                                                                                                                                                            | **Onderdeel van Modelgegevensset?**                                                                            |
| De zorgvrager vindt het belangrijk om door medewerkers geholpen te worden die hem/ haar goed kennen. Te veel wisselende gezichten kan onrust en onveiligheid bij de zorgvrager veroorzaken.   | Aantal verschillende medewerkers (verzorging/verpleging, leerlingen, stagiaires en/of vrijwilligers) voor het totaal aantal cliënten per vestiging gemiddeld per maand   | Ja, uit de praktijktoets zal blijven of deze indicator geschikt is om de cliëntvraag voldoende te beantwoorden |
| **2.2 Moet ik als cliënt niet te lang wachten als ik hulp nodig heb of vragen heb?**                                                                                                          |                                                                                                                                                                          |                                                                                                                |
| Zorgvragers ervaren dat ze niet te lang moeten wachten op hulp of een vraag. Reageren de medewerkers adequaat?                                                                                |                                                                                                                                                                          | Nee                                                                                                            |
| **2.3.1 Wie wordt mijn hoofdbehandelaar op deze locatie?**                                                                                                                                    |                                                                                                                                                                          |                                                                                                                |
| Voor zorgvrager is het belangrijk om te weten hoe de behandeling georganiseerd is.                                                                                                            | Soort hoofdbehandelaar per cliënt per vestiging                                                                                                                          | Ja, uit de praktijktoets zal blijven of deze indicator geschikt is om de cliëntvraag voldoende te beantwoorden |
| **2.3.2 Kunnen mijn paramedische behandelingen plaatsvinden op de locatie zelf?**                                                                                                             |                                                                                                                                                                          |                                                                                                                |
| Zorgvragers vinden het belangrijk om de paramedische behandelingen dichtbij te laten plaatsvinden.                                                                                            |                                                                                                                                                                          | Nee                                                                                                            |

## Thema: locatie

|                                                                                                                                |                                                                                     |                                     |
|--------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------|-------------------------------------|
| **3.1 Hoeveel bewoners kunnen er terecht op deze locatie?<br>**                                                                |                                                                                     |                                     |
| **Doel en achtergrond**                                                                                                        | **Indicator**                                                                       | **Onderdeel van Modelgegevensset?** |
| Het aantal cliënten zegt iets over de omvang van de locatie. Zorgvragers kunnen dit meenemen in het keuzeproces.               | O.b.v. VWS indicator 7.2 Het aantal verblijfplaatsen per vestiging op een peildatum | Ja                                  |
| **3.2 Welke soorten woonruimtes zijn er op de locatie incl. partnerkamers?**                                                   |                                                                                     |                                     |
| Zorgvragers krijgen inzicht in de woonruimtes zoals: eigen appartement, beschikking over een eigen keuken, partnerkamers, etc. | Soorten woonruimtes per vestiging                                                   | Ja                                  |
| **3.3 Is er een restaurant waar ikzelf of met een mantelzorger of naaste kan eten?**                                           |                                                                                     |                                     |
| Beschikt de vestiging over een restaurant waar je als zorgvrager met bezoek kan eten of koffiedrinken?                         |                                                                                     | Nee                                 |
| **3.4 Is er een tuin en/of terras?**                                                                                           |                                                                                     |                                     |
| Beschikt de vestiging over een tuin en/of terras waar je als cliënt gebruik van kan maken?                                     |                                                                                     | Nee                                 |

## Culturele identiteit/levensovertuiging

| **4.1 Welke culturele identiteit heeft deze locatie?<br>**                            |               |                                     |
|---------------------------------------------------------------------------------------|---------------|-------------------------------------|
| **Doel en achtergrond**                                                               | **Indicator** | **Onderdeel van Modelgegevensset?** |
| Mensen willen wonen op een plek waar er aandacht is voor hun gebruiken en gewoonten.  |               | Nee                                 |
| **4.2 Welke levensovertuiging heeft deze locatie?**                                   |               |                                     |
| Mensen willen wonen op een plek waar aandacht is voor hun rituelen.                   |               | Nee                                 |

## Wachttijd

|                                                                                                                      |                                                                                      |                                     |
|----------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|-------------------------------------|
| **5.1 Wat is de wachttijd voor mij met mijn Wlz-indicatie bij deze locatie?<br>**                                    |                                                                                      |                                     |
| **Doel en achtergrond**                                                                                              | **Indicator**                                                                        | **Onderdeel van Modelgegevensset?** |
| Voor zorgvragers is het belangrijk om een beeld te hebben hoe lang ze moeten wachten voor ze in zorg genomen worden. | De gemiddelde wachttijd voor cliënten met een Wlz-indicatie (in weken) per vestiging | Ja                                  |

## Communicatie

|                                                                                                                                        |               |                                     |
|----------------------------------------------------------------------------------------------------------------------------------------|---------------|-------------------------------------|
| **6.1 Hoe kan ik weten hoe het gaat met cliënt/ familielid?<br>**                                                                      |               |                                     |
| **Doel en achtergrond**                                                                                                                | **Indicator** | **Onderdeel van Modelgegevensset?** |
| Voor mantelzorgers en naasten is het belangrijk om te weten hoe het met hun verwant gaat. Hoe worden ze hiervan op de hoogte gehouden? |               | Nee                                 |

## Overig

|                                                                                                                      |                                                                                                                                                  |                                     |
|----------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------|
| **7.1 Hoe is de palliatieve zorg georganiseerd? Wordt er rekening gehouden met mijn wensen rondom levenseinde?<br>** |                                                                                                                                                  |                                     |
| **Doel en achtergrond**                                                                                              | **Indicator**                                                                                                                                    | **Onderdeel van Modelgegevensset?** |
| Voor zorgvragers en verwanten is het belangrijk dat rekening gehouden wordt m.b.t. wensen rondom het levenseinde.    | ZIN aanlevering ODB Indicator 2. Aantal gezamenlijke afspraken over behandeling rondom het levenseinde per vestiging                             | Ja                                  |
| **7.2 Wat is de link naar het laatste kwaliteitsverslag van de zorgaanbieder?**                                      |                                                                                                                                                  |                                     |
|                                                                                                                      | ZIN aanlevering ODB                                                                                                                              | Ja                                  |
| **7.3 Wordt er rekening gehouden met mijn voedselvoorkeuren?**                                                       |                                                                                                                                                  |                                     |
| Voor zorgvragers is het belangrijk dat rekening wordt gehouden met de voedselvoorkeuren.                             | ZIN aanlevering ODB Indicator 6. Aantal cliënten waarbij voedselvoorkeuren in de afgelopen 6 maanden is besproken en is vastgelegd per vestiging | Ja                                  |

## Definities informatievragen

**Algemene uitgangspunten**

Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de voorbeeldberekening van de betreffende indicator beschreven.

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten:

[Algemene uitgangspunten](https://kik-v-publicatieplatform.nl/documentatie/Algemene%20uitgangspunten)


**Specifieke uitgangspunten**

Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel. Indicator-specifieke uitgangspunten gaan beschreven worden in de functionele beschrijving van een informatievraag of indicator die in de komende periode wordt opgepakt. 

NTB

## Benodigde gegevenselementen
De concepten, eigenschappen en relaties die nodig zijn om de indicatoren te beantwoorden staan hier: NTB

## Berekeningen
Berekening van de informatievragen vindt plaats onder verantwoordelijkheid van de zorgaanbieder. De zorgaanbieder is verantwoordelijk voor de aggregatie naar het gewenste niveau.

Klik hier voor de (voorbeeld)berekeningen indicatoren Cliëntinformatie: NTB