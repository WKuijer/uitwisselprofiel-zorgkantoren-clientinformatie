---
title: Organisatorische interoperabiliteit
weight: 2
---
## Gewenste momenten van aanlevering
De resultaten dienen actueel beschikbaar te zijn. Afhankelijk van de interne processen bij de zorgaanbieders zijn deze minimaal 1 dag na de wijziging van het gegeven beschikbaar: zie Semantische laag voor verdere details.

## Gewenste momenten van terugkoppeling
Er vindt geen directe terugkoppeling plaats van de gegevens. Zorgaanbieders zien via de verschillende zorgbemiddelingswebsites van de zorgkantoren, ZorgKaartNederland en Keuzehulp verpleeghuiszorg gegevens terug.

## Looptijd
De looptijd van het uitwisselprofiel is continu doorlopend tot het moment van wijziging.

## Gewenste bewaartermijn
NTB

## Afspraken bij twijfels over de kwaliteit van gegevens
Als een zorgkantoor vragen heeft over de interpretatie van de gegevens en/of over de kwaliteit van gegevens, neemt het zorgkantoor contact op met de betreffende zorgaanbieder. Indien de partijen, die gegevens door geleverd krijgen, vragen hebben over de interpretatie dan wel de kwaliteit van de gegevens, bespreken zij deze met de zorgkantoren.

## Afspraken over een eventuele mogelijkheid tot nalevering en correctie
De gegevens kunnen door de zorgaanbieder worden aangepast en zo snel mogelijk verbeterd worden klaargezet.

## In- en exclusiecriteria
De uitvraag is bedoeld voor zorgorganisaties die onder onderstaande inclusiecriteria vallen:

* Doelgroep
    * Tot de doelgroep behoren alle instellingen met een Wlz contract
    * Tot de doelgroep behoren alle instellingen binnen een zorgkantoorregio afbakening (per concessiehouder)

* Profiel (inclusiecriteria)
    * AGB-code organisatie (verwijzing naar Wlz VV)
    * De profielen Wet langdurige zorg 