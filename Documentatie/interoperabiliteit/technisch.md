---
title: Technische interoperabiliteit
weight: 4
---
Indien een zorgaanbieder kan berekenen en aanleveren via een datastation, dan kunnen de antwoorden worden aangeleverd middels de KIK-starter. Voor meer informatie [klik hier](https://www.kik-v.nl/onderwerpen/producten). De zorgaanbieder gebruikt hiervoor de volgende SPARQL-queries: NTB.

Voor aanlevering middels de KIK-starter gelden de volgende afspraken en nadere fasering voor dit uitwisselprofiel: NTB