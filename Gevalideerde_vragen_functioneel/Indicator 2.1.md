+++
title = "2.1. Welke paramedische behandelingen kunnen plaatsvinden op deze vestiging?"
+++

## DIT IS EEN UITWERKING OM DE DIALOOG TE VOEDEN, (DUS NOG GEEN FUNCTIONELE BESCHRIJVING)

**Opmerking:** Om de dialoog te voeden staan ook vragen en discussie-onderwerpen in dit concept. Die worden beslecht en verwijderd voordat dit een als functionele beschrijving voor deze indidactor kan gelden.

## Definitie

**Definitie:** Op meetmoment beschikbare paramedische zorg per vestiging.

## Toelichting

De informatievraag betreft de paramedische zorg die per vestiging geleverd kan worden. Dit betreft de bereidheid (en en daarmee de geschiktheid) om op die vestiging de gerapporteerde paramedische zorg te leveren onafhankelijk van actuele of historische leveringen.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* Het beleidsmatige voornemen om op of vanuit bepaalde vestigingen bepaalde paramedische zorg te (willen) leveren, en daarmee het aanbod op de markt wordt door de zorgorganisatie in tabelvorm gevoed (bronsysteem). Dit zorgaanbod wordt uitgedrukt in één of meer van de volgende soorten paramedische zorg [ZorgInstituut](https://www.zorginstituutnederland.nl/Verzekerde+zorg/paramedische-zorg-zvw):
* Regimes/therapieën
  * Fysiotherapie - [SCTID: 91251008](https://browser.ihtsdotools.org/?perspective=full&conceptId1=91251008&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), fysiotherapie (regime/therapie)
  * Oefentherapie - [SCTID: 229065009](https://browser.ihtsdotools.org/?perspective=full&conceptId1=229065009&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), oefentherapie (regime/therapie)
  * Logopedie - [SCTID: 5154007](https://browser.ihtsdotools.org/?perspective=full&conceptId1=5154007&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), logopedische behandeling (regime/therapie)
  * Ergotherapie - [SCTID: 84478008](https://browser.ihtsdotools.org/?perspective=full&conceptId1=84478008&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), ergotherapie (regime/therapie)
* Verrichtingen
  * **twijfelachtig:** Diëtetiek - [SCTID: 424753004](https://browser.ihtsdotools.org/?perspective=full&conceptId1=424753004&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), adviseren, instrueren en/of begeleiden betreffende dieetmanagement (verrichting)
