+++
title = "3.2 Welke soorten woonruimtes zijn er op de locatie incl. partnerkamers?"
definitie = "Het aantal wooneenheden voor cliënten en eventueel een partner waarover een zorgaanbieder per vestiging en op organisatieniveau op een peildatum beschikt."
noemer = "Niet van toepassing."
teller = "Aantal kamers geschikt voor verblijf."
+++

# DEZE FUNCTIONELE BESCHRIJVING IS IN ONTWIKKELING


## Toelichting
Deze indicator geeft de capaciteit van een zorgaanbieder op een bepaald moment aan. De capaciteit betreft het aantal wooneenheden dat geschikt is voor cliënten en eventueel een partner. 

Deze indicator wordt op een peildatum op organisatieniveau en per vestiging berekend. 


## Uitgangspunten

* Een wooneenheid betreft een locatie in een vestiging waar een of meerdere cliënten met eventueel een partner kunnen wonen. Dit betreft bijvoorbeeld een kamer of appartement die geschikt is voor een of meer personen, zoals een cliënt en eventueel de partner van een cliënt.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle wooneenheden op de peildatum.  
2. Bepaal per wooneenheid de vestiging.
3. Bereken het aantal wooneenheid per vestiging en voor de totale organisatie.

Peildatum: 
| Capaciteit m.b.t.: | Aantal wooneenheden | 
|----------------|--------|
| Totaal organisatie | Stap 3 | 
| Vestiging 1 | Stap 3 | 
| Vestiging 2 | Stap 3 | 
| Vestiging N | Stap 3 | 
