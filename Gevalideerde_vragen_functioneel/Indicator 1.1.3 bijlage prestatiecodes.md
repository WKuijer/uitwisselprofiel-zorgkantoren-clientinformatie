# Bijlage: Relatie tussen vormen van zorg en prestatiecodes

Bijlage bij: [Indicator 1.1.3.md](./Indicator 1.1.3.md)

Bron: [BR/REG-23121a, art. 7](https://puc.overheid.nl/doc/PUC_723923_22/1/#a67bc9c8-268c-4c1e-88b3-5be298db9dc8).

| Niveau 1 | Niveau 2 | Prestatie | Prestatie-code | Tabel nr | Omschrijving bij tabel, site Nza |
|---|---|---|---|---|---|
| 1. Huishoudelijke hulp |  |  |  |  |  |
|  |  | Huishoudelijke hulp* | H117 | 1 | Huishoudelijke hulp |
| 2. Persoonlijke verzorging (pv) |  |  |  |  |  |
|  |  | Persoonlijke verzorging | H126 | 2 | Persoonlijke verzorging (pv) |
|  |  | Persoonlijke verzorging incl. beschikbaarheid | H127 | 2 | Persoonlijke verzorging (pv) |
|  |  | Thuiszorgtechnologie ten behoeve van persoonlijke verzorging | H138 | 2 | Persoonlijke verzorging (pv) |
|  |  | Persoonlijke verzorging Speciaal | H120 | 2 | Persoonlijke verzorging (pv) |
| 3. Begeleiding Individueel en begeleiding in groepsverband |  |  |  |  |  |
|  | 3.a. Begeleiding Individueel |  |  |  |  |
|  |  | Begeleiding | H300 | 3 | Begeleiding individueel in uren |
|  |  | Thuiszorgtechnologie ten behoeve van begeleiding | H306 | 3 | Begeleiding individueel in uren |
|  |  | Begeleiding incl. beschikbaarheid | H150 | 3 | Begeleiding individueel in uren |
|  |  | Begeleiding speciaal 1 (nah) | H152 | 3 | Begeleiding individueel in uren |
|  |  | Begeleiding speciaal 2 (psy) | H153 | 3 | Begeleiding individueel in uren |
|  |  | Begeleiding zg visueel | H301 | 3 | Begeleiding individueel in uren |
|  |  | Begeleiding zg auditief | H303 | 3 | Begeleiding individueel in uren |
|  |  | Begeleiding speciaal 2 (visueel) | H302 | 3 | Begeleiding individueel in uren |
|  |  | Begeleiding speciaal 2 (auditief) | H304 | 3 | Begeleiding individueel in uren |
|  |  | Nachtverzorging | H132 | 4 | Begeleiding in dagdelen |
|  |  | Nachtverpleging | H180 | 4 | Begeleiding in dagdelen |
|  | 3.b Begeleiding in groepsverband |  |  |  |  |
|  |  | Dagbesteding basis | H531 | 5 | Ouderen |
|  |  | Dagbesteding somatisch ondersteunend | H800 | 5 | Ouderen |
|  |  | Dagbesteding psychogeriatrisch | H533 | 5 | Ouderen |
|  |  | Dagbesteding vg licht | H811 | 6 | Verstandelijk gehandicapt |
|  |  | Dagbesteding vg midden | H812 | 6 | Verstandelijk gehandicapt |
|  |  | Dagbesteding vg zwaar | H813 | 6 | Verstandelijk gehandicapt |
|  |  | Dagbesteding vg kind licht | H814 | 6 | Verstandelijk gehandicapt |
|  |  | Dagbesteding vg kind midden | H815 | 6 | Verstandelijk gehandicapt |
|  |  | Dagbesteding vg kind zwaar | H816 | 6 | Verstandelijk gehandicapt |
|  |  | Dagbesteding vg kind gedrag | H818 | 6 | Verstandelijk gehandicapt |
|  |  | Dagbesteding lg licht | H831 | 7 | Lichamelijk gehandicapt |
|  |  | Dagbesteding lg midden | H832 | 7 | Lichamelijk gehandicapt |
|  |  | Dagbesteding lg zwaar | H833 | 7 | Lichamelijk gehandicapt |
|  |  | Dagbesteding lg kind licht | H834 | 7 | Lichamelijk gehandicapt |
|  |  | Dagbesteding lg kind midden | H835 | 7 | Lichamelijk gehandicapt |
|  |  | Dagbesteding lg kind zwaar | H836 | 7 | Lichamelijk gehandicapt |
|  |  | Dagbesteding ggz Wonen-1 | H001G | 8 | Ggz Wonen |
|  |  | Dagbesteding ggz Wonen-2 | H002G | 8 | Ggz Wonen |
|  |  | Dagbesteding ggz Wonen-3 | H003G | 8 | Ggz Wonen |
|  |  | Dagbesteding ggz Wonen-4 | H004G | 8 | Ggz Wonen |
|  |  | Dagbesteding ggz Wonen-5 | H005G | 8 | Ggz Wonen |
|  |  | Dagbesteding zg auditief licht | H851 | 9 | Zintuiglijk gehandicapt auditief |
|  |  | Dagbesteding zg auditief midden | H852 | 9 | Zintuiglijk gehandicapt auditief |
|  |  | Dagbesteding zg auditief zwaar | H853 | 9 | Zintuiglijk gehandicapt auditief |
|  |  | Dagbesteding zg kind auditief licht | H854 | 9 | Zintuiglijk gehandicapt auditief |
|  |  | Dagbesteding zg kind auditief midden | H855 | 9 | Zintuiglijk gehandicapt auditief |
|  |  | Dagbesteding zg kind auditief zwaar | H856 | 9 | Zintuiglijk gehandicapt auditief |
|  |  | Dagbesteding zg visueel licht | H871 | 10 | Zintuiglijk gehandicapt visueel |
|  |  | Dagbesteding zg visueel midden | H872 | 10 | Zintuiglijk gehandicapt visueel |
|  |  | Dagbesteding zg visueel zwaar | H873 | 10 | Zintuiglijk gehandicapt visueel |
|  |  | Dagbesteding zg kind visueel licht | H874 | 10 | Zintuiglijk gehandicapt visueel |
|  |  | Dagbesteding zg kind visueel midden | H875 | 10 | Zintuiglijk gehandicapt visueel |
|  |  | Dagbesteding zg kind visueel zwaar | H876 | 10 | Zintuiglijk gehandicapt visueel |
|  |  | Dagbesteding lza | F125 | 11 | Langdurig zorg afhankelijk |
| 4. Verpleging (vp) |  |  |  |  |  |
|  |  | Verpleging | H104 | 12 | Verpleging (vp) |
|  |  | Thuizorgtechnologie ten behoeve van verpleging | H139 | 12 | Verpleging (vp) |
|  |  | Verpleging incl. beschikbaarheid | H128 | 12 | Verpleging (vp) |
|  |  | Verpleging speciaal | H106 | 12 | Verpleging (vp) |
|  |  | Verpleging speciaal aan kinderen tot 18 jaar incl. beschikbaarheid | H118 | 12 | Verpleging (vp) |
|  |  | Verpleging speciaal aan kinderen tot 18 jaar excl. Beschikbaarheid | H119 | 12 | Verpleging (vp) |
| 5. Behandeling individueel en behandeling in een groep (dagbehandeling) |  |  |  |  |  |
|  | 5.a. Behandeling individueel |  |  |  |  |
|  |  | Behandeling som, pg, vg, lg, zg (so) | H335 | 13 | Behandeling individueel |
|  |  | Behandeling som, pg, vg, lg, zg (avg) | H336 | 13 | Behandeling individueel |
|  |  | Behandeling gedragswetenschapper | H329 | 13 | Behandeling individueel |
|  |  | Behandeling paramedisch | H330 | 13 | Behandeling individueel |
|  |  | Behandeling lvg | H325 | 13 | Behandeling individueel |
|  |  | Behandeling IOG lvg | H334 | 13 | Behandeling individueel |
|  |  | Behandeling sglvg | H338 | 13 | Behandeling individueel |
|  |  | Behandeling Families First lvg | H331 | 13 | Behandeling individueel |
|  |  | Behandeling zg visueel | H332 | 13 | Behandeling individueel |
|  |  | Behandeling zg auditief | H333 | 13 | Behandeling individueel |
|  |  | Medische verklaring in het kader van de Wet zorg en dwang – rechterlijke machtiging | H353 | 14 | Werkzaamheden in het kader van de Wet zorg en dwang (Wzd) |
|  |  | Medische verklaring in het kader van de Wet zorg en dwang – inbewaringstelling | H354 | 14 | Werkzaamheden in het kader van de Wet zorg en dwang (Wzd) |
|  |  | Beoordeling tot inbewaringstelling zonder afgifte medische verklaring in het kader van de Wet zorg en dwang | H355 | 14 | Werkzaamheden in het kader van de Wet zorg en dwang (Wzd) |
|  |  | Verschijnen ter zitting in het kader van de Wet zorg en dwang | H356 | 14 | Werkzaamheden in het kader van de Wet zorg en dwang (Wzd) |
|  |  | Reistoeslag zorgverlener bij verschijnen ter zitting in het kader van de Wet zorg en dwang, per 10 minuten | H357 | 14 | Werkzaamheden in het kader van de Wet zorg en dwang (Wzd) |
|  | 5.b. Behandeling in een groep |  |  |  |  |
|  |  | Dagbehandeling ouderen som en pg | H802 | 15 | Ouderen |
|  |  | Gespecialiseerde dagbehandeling Huntington lg of som | H804 | 16 | Huntington |
|  |  | Dagbehandeling vg emg volwassenen | H819 | 17 | Verstandelijk gehandicapt |
|  |  | Dagbehandeling vg kind midden | H820 | 17 | Verstandelijk gehandicapt |
|  |  | Dagbehandeling vg kind zwaar | H821 | 17 | Verstandelijk gehandicapt |
|  |  | Dagbehandeling vg kind emg | H817 | 17 | Verstandelijk gehandicapt |
|  |  | Dagbehandeling vg kind gedrag | H822 | 17 | Verstandelijk gehandicapt |
|  |  | Dagbehandeling lvg | H891 | 18 | Licht verstandelijk gehandicapt |
|  |  | Dagbehandeling lg | H840 | 19 | Lichamelijk gehandicapt |
| 6. Vervoer dagbesteding en dagbehandeling |  |  |  |  |  |
|  |  | Vervoer dagbesteding/dagbehandeling vv – categorie 0* | H8030 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling vv – categorie 1 | H8031 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling vv – categorie 2** | H8032 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling vv – categorie 3** | H8033 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling vv – categorie 4** | H8034 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling vv – categorie 5** | H8035 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling vv – categorie 6** | H8036 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ghz – categorie 0 | H886 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ghz – categorie 1 | H881 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ghz – categorie 2 | H882 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ghz – categorie 3 | H883 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ghz – categorie 4 | H884 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ghz – categorie 5 | H885 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ghz – categorie 6 | H887 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ggz Wonen – categorie 0 | H410 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ggz Wonen – categorie 1 | H411 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ggz Wonen – categorie 2 | H412 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ggz Wonen – categorie 3 | H413 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ggz Wonen – categorie 4 | H414 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ggz Wonen – categorie 5 | H415 | 20 | Vervoer dagbesteding/dagbehandeling |
|  |  | Vervoer dagbesteding/dagbehandeling ggz Wonen – categorie 6 | H416 | 20 | Vervoer dagbesteding/dagbehandeling |
