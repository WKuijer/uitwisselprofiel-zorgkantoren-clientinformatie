---
title = "1.3. Met welke problematiek kunt u op deze vestiging terecht?"
---

**Status:** onder constructie

# DEZE FUNCTIONELE BESCHRIJVING IS IN ONTWIKKELING

**Opmerking:** Om de dialoog te voeden staan ook vragen en discussie-onderwerpen in dit concept. Die worden beslecht en verwijderd voordat dit een als functionele beschrijving voor deze indidactor kan gelden.

**Vraag:** Nu per vestiging uitgewerkt, is dit (ook) relevant voor per woonplaats?

**Vraag:** Hoe kunnen we differentieren per leeftijdscategorie? (NAH); 

## Definitie

**Definitie:** Problematiek vaarvoor op meetmoment cliënten bij deze vestiging terecht kunnen .

## Toelichting

De informatievraag betreft per vestiging de problematie(en) waarvoor aan cliënten bij deze vestiging terecht kunnen. Dit betreft de bereidheid (en en daarmee de geschiktheid) om op deze vestiging voor  cliënten met dergelijke problematiek passende zorg te leveren onafhankelijk van actuele of historische leveringen.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* Het beleidsmatige voornemen dat cliënten met bepaalde problematiek op een vestiging terecht kunnen, en daarmee het aanbod op de markt wordt door de zorgorganisatie in tabelvorm gevoed (bronsysteem).
* Hieronder een (eerste) poging om op basis van SNOMED-CT problematieken te duiden. Omdat niet van alle genoemde termen een aandoening gevonden is als alternatieven in prioriteitsvolgorde gekozen voor bevinding of regime/therapie.
* Aandoeningen  
  * Beroerte (CVA) - [SCTID: 230690007](https://browser.ihtsdotools.org/?perspective=full&conceptId1=230690007&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), cerebrovasculair accident (aandoening)
  * Dementie < 65 jaar - [SCTID: 52448006](https://browser.ihtsdotools.org/?perspective=full&conceptId1=52448006&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), leeftijd >= 65 jaar, dementie (aandoening)
  * Dementie > 65 jaar - [SCTID: 52448006](https://browser.ihtsdotools.org/?perspective=full&conceptId1=52448006&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), leeftijd < 65 jaar, dementie (aandoening)
  * Dementie met zeer ernstige gedragsproblematiek - [SCTID: 52448006](https://browser.ihtsdotools.org/?perspective=full&conceptId1=52448006&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), dementie (aandoening), **hoe specificeren we de gedragsproblematiek?**
  * Hartfalen - [SCTID: 195111005](https://browser.ihtsdotools.org/?perspective=full&conceptId1=195111005&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), hartdecompensatie (aandoening)
  * Longziekte (w.o. COPD) - [SCTID: 19829001](https://browser.ihtsdotools.org/?perspective=full&conceptId1=19829001&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), aandoening van long (aandoening)
  * Multipele Sclerose - [SCTID: 24700007](https://browser.ihtsdotools.org/?perspective=full&conceptId1=24700007&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), multipele sclerose (aandoening)
  * Niet-aangeboren hersenletsel (NAH) > 65 jaar - [SCTID: 114051000146102](https://browser.ihtsdotools.org/?perspective=full&conceptId1=114051000146102&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), leeftijd >= 65 jaar,  niet-aangeboren hersenletsel (aandoening)
  * Niet-aangeboren hersenletsel (NAH) < 65 jaar- [SCTID: 114051000146102](https://browser.ihtsdotools.org/?perspective=full&conceptId1=114051000146102&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), leeftijd < 65 jaar,  niet-aangeboren hersenletsel (aandoening)
  * Niet-aangeboren hersenletsel (NAH) met bijkomende problematiek - [SCTID: 114051000146102](https://browser.ihtsdotools.org/?perspective=full&conceptId1=114051000146102&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en),  niet-aangeboren hersenletsel (aandoening), **hoe specificeren we bijkomende problematiek?**
  * Reumatische aandoening - [SCTID: 396332003](https://browser.ihtsdotools.org/?perspective=full&conceptId1=396332003&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), reuma (aandoening)
  * Spierziekte (w.o. ALS) - [SCTID: 129565002](https://browser.ihtsdotools.org/?perspective=full&conceptId1=129565002&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en),myopathie (aandoening)
  * Ziekte van Huntington - [SCTID: 702376003](https://browser.ihtsdotools.org/?perspective=full&conceptId1=702376003&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en),  ziekte van Huntington-achtig syndroom (aandoening)
  * Ziekte van Korsakov - [SCTID: 69482004](https://browser.ihtsdotools.org/?perspective=full&conceptId1=69482004&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), Korsakov-syndroom (aandoening)
  * Ziekte van Parkinson - [SCTID: 32798002](https://browser.ihtsdotools.org/?perspective=full&conceptId1=32798002&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en),  parkinsonisme (aandoening)
* Bevindingen
  * Afasie - [SCTID: 87486003](https://browser.ihtsdotools.org/?perspective=full&conceptId1=87486003&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), afasie (bevinding)
  * Kwetsbare ouderen meervoudige problematiek [SCTID: 404904002](https://browser.ihtsdotools.org/?perspective=full&conceptId1=404904002&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), kwetsbare oudere (bevinding), **is dit wellicht te generiek?**
* Regimes / therapieën
  * Ademhalingsondersteuning (invasief) - geen passende SCTID gevonden
    * Alternatief?: [CTID: 40617009](https://browser.ihtsdotools.org/?perspective=full&conceptId1=40617009&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), kunstmatige beademing (regime/therapie)
  * Ademhalingsondersteuning (non-invasief) - [SCTID: 428311008](https://browser.ihtsdotools.org/?perspective=full&conceptId1=428311008&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), non-invasieve beademing (regime/therapie)
  * Laatste (terminale) levensfase [SCTID: 182964004](https://browser.ihtsdotools.org/?perspective=full&conceptId1=182964004&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en), terminale zorg (regime/therapie)
* Onduidelijk
  * Langdurig bewustzijnsstoornis
  * Gerontopsychiatrie
