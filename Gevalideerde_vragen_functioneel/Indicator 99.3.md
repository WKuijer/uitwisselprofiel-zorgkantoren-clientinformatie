---
title: 99.3 Webadres kwaliteitsverslag 2023.
description: "URL van de website waar het Kwaliteitsverslag 2023 van de zorgorganisatie te vinden is."
weight: 1
---
## Indicator

**Definitie:** URL van de website waar het Kwaliteitsverslag 2023 van de zorgorganisatie te vinden is.

**Teller:** Niet van toepassing.

**Noemer:** Niet van toepassing.

## Toelichting

De zorgaanbieder vult handnmatig de URL van de website waar het kwaliteitsverslag 2023 van de zorgaanbeider te vinden is in.

## Uitgangspunten

* Het betreft de URL van de website van het kwaliteitsverslag 2023 van de zorgaanbeider.

## Berekening

Niet van toepassing.
