+++
title = "1.4. Welke vestigingen zijn als Wzd- of Wvggz-vestiging geregistreerd?"
+++

## Definitie

**Definitie:** Op de peildatum per vestiging of de betreffende vestiging als Wzd- en/of Wvggz-vestiging is geregistreerd in het locatieregister.

## Toelichting

De informatievraag betreft welke vestigingen op de peildatum als Wzd- en/of Wvggz-vestiging staan geregistreerd in het locatieregister.

## Uitgangspunten

* Of een vestiging een WZD- en/of Wvggz-vestiging betreft wordt bepaald o.b.v. de registratie van de vestiging als WZD- of Wvggz-locatie in het locatie-register. NB: In het kennismodel van KiK-V wordt een 'locatie' een 'vestiging' genoemd. _Een vestiging is een gebouw of complex van gebouwen waar duurzame uitoefening van de activiteiten van een onderneming of rechtspersoon plaatsvindt_.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle vestigingen.
2. Bepaal per vestiging of deze op de peildatum als Wzd- en/of Wvggz-locatie is geregistreerd. Rapporteer als dat het geval is per wet 'ja', en anders per wet 'nee'.

Peildatum: dd-mm-jjjj

| Organisatieonderdeel | geregistreerd als Wzd-vestiging? | geregistreerd als Wvggz-vestiging? |
| --- | --- | --- |
| Vestiging 1 | stap 2 | stap 2 |
| Vestiging 2 | stap 2 | stap 2 |
| Vestiging N | stap 2 | stap 2 |
