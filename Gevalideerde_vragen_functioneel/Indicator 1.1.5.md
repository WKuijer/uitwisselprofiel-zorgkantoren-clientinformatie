---
title: = 1.1.5. In welke woonplaats(en) kan deze organisatie zorg thuis ook middels een Persoonsgebonden Budget (PGB) gefinancieerd leveren?
weight: n.t.b.
---

## Indicator

**Definitie:** Woonplaatsen waar op meetmoment zorg thuis geleverd kan worden bij leveringsvorm Persoonsgebonden Budget (PGB)

## Toelichting

De informatievraag betreft de woonplaatsen waar zorg thuis geleverd kan worden via leveringsvorm PGB. Dit betreft de bereidheid (en en daarmee de geschiktheid) om in die woonplaats zorg thuis via PGB te leveren onafhankelijk van actuele of historische leveringen.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* Het beleidsmatige voornemen om in bepaalde woonplaats(en) zorg thuis, gefinancierd middels een PGB, te (willen) leveren, en daarmee het aanbod op de markt wordt door de zorgorganisatie in tabelvorm gevoed (bronsysteem). Dit zorgaanbod wordt uitgedrukt in de zorg die gespecificeerd is in [Bijlage 1 bij BR-REG-23121a Prestatiebeschrijvingen.pdf](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_723923_22_1&bestand=Bijlage_1_bij_BR-REG-23121a_Prestatiebeschrijvingen.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-23121a+Prestatiebeschrijvingen.pdf), met uitzondering van "Behandeling individueel" en "Behandeling in een groep (dagbehandeling)". Concreet zijn de volgende prestatiecodes uitgesloten: H335, , H336, H329, H330, H325, H334, H338, H331, H332, H333, H353, H354, H355, H356, H357, H802, H804, H819, H820, H821, H817, H822, H891, H840.
* Als in een woonplaats minimaal één van de bovengenoemde prestaties geleverd kan worden dan telt die woonplaats mee.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer  alle _woonplaatsen_ (rijen) waar zorg thuis via een PGB geleverd kan worden.
2. Sorteer op alfabet.

Meetmoment: dd-mm-jjjj

| Woonplaats           |
| ---                  |
| Woonplaats 1         |
| Woonplaats 2         |
| ...                  |
| Woonplaats n         |

## Ter bespreking

| No. | Soort | Omschrijving | Bron / Link |
|-----|-------|--------------|-------------|
| 1   | Opmerking |          |             |
