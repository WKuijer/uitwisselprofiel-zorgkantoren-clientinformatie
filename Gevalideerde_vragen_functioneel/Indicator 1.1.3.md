---
title: = 1.1.3. In welke woonplaats(en) kan deze organisatie welk zorgaanbod leveren bij leveringsvorm Modulair Pakket Thuis (MPT)?
weight: n.t.b.
---

## Indicator

**Definitie:** Op meetmoment beschikbaar zorgaanbod bij leveringsvorm Modulair Pakket Thuis (MPT) per woonplaats.

## Toelichting

De informatievraag betreft het zorgaanbod bij leveringsvorm MPT dat per woonplaats geleverd kan worden. Dit betreft de bereidheid (en en daarmee de geschiktheid) om in die woonplaats het gerapporteerde zorgaanbod te leveren onafhankelijk van actuele of historische leveringen.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* Het beleidsmatige voornemen om in bepaalde woonplaats(en) bepaalde zorg in bepaalde leveringsvormen te (willen) leveren, en daarmee het aanbod op de markt wordt door de zorgorganisatie in tabelvorm gevoed (bronsysteem). Dit zorgaanbod wordt bij leveringsvorm MPT uitgedrukt in de zorg die gespecificeerd is in één of meer van de volgende verzamelingen van _prestaties_ (In het overzicht hieronder is Persoonlijke verzorging de verzameling prestaties H126, H127, H138, H120):
  * 1.**Huishoudelijke hulp** (BR/REG-23121a, bijlage 1, H117)
  * 2.**Persoonlijke verzorging** (BR/REG-23121a, bijlage 1, H126, H127, H138, H120)
  * 3.a.**Begeleiding individueel** (BR/REG-23121a, bijlage 1, H300, H306, H150, H152, H153, H301, H303, H302, H304, H132, H180)
  * H104.**Verpleging** (BR/REG-23121a, bijlage 1, H104)
  * H139.**Thuizorgtechnologie ten behoeve van verpleging** (BR/REG-23121a, bijlage 1, H139)
  * H128.**Verpleging incl. beschikbaarheid** (BR/REG-23121a, bijlage 1, H138)
  * H106.**Verpleging speciaal** (BR/REG-23121a, bijlage 1, H106)
  * H118.**Verpleging speciaal aan kinderen tot 18 jaar incl. beschikbaarheid** (BR/REG-23121a, bijlage 1, H118)
  * H119.**Verpleging speciaal aan kinderen tot 18 jaar excl. Beschikbaarheid** (BR/REG-23121a, bijlage 1, H119)
  * 5.a **Behandeling individueel** (BR/REG-23121a, bijlage 1, H335, H336, H329, H330, H325, H334, H338, H331, H332, H333, H353, H354, H355, H356, H357)
  * 6.**Vervoer (naar begeleiding en/of behandeling)** (BR/REG-23121a, bijlage 1, H8030, H8031, H8032, H8033, H8034, H8035, H8036, , 886, H881, H882, H883, H884, H885, H887, H410, H411, H412, H413, H414, H415, H416)
* [Indicator 1.1.3. bijlage prestatiecodes.md](./Indicator 1.1.3 bijlage prestatiecodes.md) geeft van bovenstaande vormen van zorg  en prestatiecodes een overzicht in tabelvorm. [Bijlage 1 bij BR-REG-23121a Prestatiebeschrijvingen.pdf](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_723923_22_1&bestand=Bijlage_1_bij_BR-REG-23121a_Prestatiebeschrijvingen.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-23121a+Prestatiebeschrijvingen.pdf) biedt van iedere prestatie een nadere beschrijving van de te leveren zorg. De vetgedrukte termen in bovenstaande overzicht zijn de benamingen van de bijbehorende verzameling van _prestaties_.
* _Zorgaanbod bij leveringsvorm MPT_ is de beschikbaarheid van zorg benodigd t.b.v. bovenstaande verzamelingen van _prestaties_ geleverd via de _leveringsvorm_ MPT.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer  alle _zorgaanbod bij leveringsvorm MPT_ (kolommen). Als label wordt de naam van de verzameling _prestaties_  gebruikt.
2. Bepaal per _woonplaats_ (rijen) welk zorgaanbod bij _leveringsvorm MPT_  beschikbaar is. Rapporteer als het betreffende zorgaanbod beschikbaar is 'ja', en anders 'nee'. De vetgedrukte termen in bovenstaand overzicht worden gebruikt om het betreffende zorgaanbod aan te duiden.
3. Neem alleen woonplaatsen op in de tabel waarvoor bij minimaal één zorgprofiel 'ja' gerapporteerd wordt. Sorteer op alfabet.

Meetmoment: dd-mm-jjjj

| Woonplaats           | Zorgaanbod 1       | Zorgaanbod 2     | ...    | Zorgaanbod n     |
| ---                  | :---:              | :---:            | :---:  | :---:            |
| Woonplaats 1         | stap 2             | stap 2           | stap 2 | stap 2           |
| Woonplaats 2         | stap 2             | stap 2           | stap 2 | stap 2           |
| ...                  |                    |                  |        |                  |
| Woonplaats m         | stap 2             | stap 2           | stap 2 | stap 2           |

## Ter bespreking

| No. | Soort | Omschrijving | Bron / Link |
|-----|-------|--------------|-------------|
| 1   | Opmerking |          |             |
