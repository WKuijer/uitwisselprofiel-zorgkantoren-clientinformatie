---
title = "1.2 Welk specifiek zorgaanbod kan op deze vestiging geleverd worden?
---

**Status:** onder constructie

# DEZE FUNCTIONELE BESCHRIJVING IS IN ONTWIKKELING

**Opmerking:** Om de dialoog te voeden staan ook vragen en discussie-onderwerpen in dit concept. Die worden beslecht en verwijderd voordat dit een als functionele beschrijving voor deze indidactor kan gelden.

## Definitie

**Definitie:** Op meetmoment beschikbaar specifiek zorgaanbod bij leveringsvorm Verblijf per vestiging.

## Toelichting

De informatievraag betreft het specifieke zorgaanbod dat per vestiging geleverd kan worden. Dit betreft de bereidheid (en en daarmee de geschiktheid) om op die vestiging het gerapporteerde specifieke zorgaanbod te leveren onafhankelijk van actuele of historische leveringen.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* Het beleidsmatige voornemen om op of vanuit bepaalde vestigingen bepaalde zorg te (willen) leveren, en daarmee het aanbod op de markt wordt door de zorgorganisatie in tabelvorm gevoed (bronsysteem).
* De prestatie crisiszorg vv & lvg is beschreven in [BR/REG-23122a, artikel6, lid 4.b](https://puc.overheid.nl/doc/PUC_723961_22/1/#9d2c343d-4f11-45a1-be42-9c8ce0eea6ce).
* De prestatie logeren vg, lg, lvg, zg, vv en ggz is beschreven in [BR/REG-23122a, artikel6, lid 4.g](https://puc.overheid.nl/doc/PUC_723961_22/1/#9d2c343d-4f11-45a1-be42-9c8ce0eea6ce).
* Dementie is beschreven in SNOMED-CT: [SCTID: 52448006](https://browser.ihtsdotools.org/?perspective=full&conceptId1=52448006&edition=MAIN/SNOMEDCT-NL&release=&languages=nl,en)
* **Lichamelijke aandoening vraagt nog een definitie.**
* **vraag:** Is het mogelijk om op deze wijze te combineren?
  * Crisiszorg dementie is de beschikbaarheid van zorg zoals gespecificeerd in bovenstaande prestatie voor cllienten met problementiek dementie.
  * Ciziszorg lichamelijke aandoening - vergelijkbaar met nog te vinden definitie voor lichamelijke aandoening. Is een dergelijke generieke categorie niet (toch) ergens beschikbaar in SNOMED-CT?
  * Logeren dementie  is de beschikbaarheid van zorg zoals gespecificeerd in bovenstaande prestatie voor cllienten met problementiek dementie.
  * Logeren lichamelijke aandoeding - Vergelijkbare vraagstuk als voor crisiszorg lichamelijke aandoening hierboven.
