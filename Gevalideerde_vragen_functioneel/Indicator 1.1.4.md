---
title: 1.1.4. Welk zorgaanbod kan deze vestiging leveren bij Deeltijdverblijf (DTV)?
weight: n.t.b.
---

## Indicator

**Definitie:** Op meetmoment beschikbaar zorgaanbod bij de combinatie van leveringsvormen Deeltijdverblijf (DTV) per vestiging.

## Toelichting

De informatievraag betreft het zorgaanbod bij de combinatie van leveringsvormen DTV dat per vestiging geleverd kan worden. Dit betreft de bereidheid (en en daarmee de geschiktheid) om op die vestiging het gerapporteerde zorgaanbod te leveren onafhankelijk van actuele of historische leveringen.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* Het beleidsmatige voornemen om op of vanuit bepaalde vestigingen bepaalde zorg in bepaalde leveringsvormen te (willen) leveren, en daarmee het aanbod op de markt wordt door de zorgorganisatie in tabelvorm gevoed (bronsysteem). Dit zorgaanbod wordt bij leveringsvorm DTV uitgedrukt in Zorgprofielen.
* DTV (Deeltijdverblijf) is [een combinatie van thuis wonen en in een Wlz-instelling wonen](http://purl.org/ozo/onz-zorg#Leveringsvorm). Dit is dus een _combinatie van leveringsvormen_ die de organisatie bereid is te leveren aan één persoon. Concreet zijn er drie combinaties mogelijk: (Verblijf & MPT), (Verblijf & PGB) en (Verblijf & MPT & PGB). Deze indicator beschrijft van deze combinatie de component Verblijf, de beschikbare zorg uit de andere component(en) wordt beschreven in [Indicator 1.1.3. "Welk zorgaanbod kan deze vestiging leveren bij Modulair Pakket Thuis (MPT)?"](./Indicator 1.1.3.md) en [Indicator 1.1.5. "Welk zorgaanbod kan deze vestiging leveren bij Persoonsgebonden Budget (PGB)?"](./Indicator 1.1.5.md)".
* _Zorgaanbod bij combinatie van leveringsvormen DTV_ is de beschikbaarheid van zorg benodigd t.b.v. onderstaande _zorgprofielen_ geleverd via de _combinatie van leveringsvormen_ DTV.
  * [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV) (vv-4)
  * [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV) (vv-5)
  * [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV) (vv-6)
  * [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV) (vv-7)
  * [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV) (vv-8)
  * [LG Wonen met begeleiding en enige verzorging](http://purl.org/ozo/onz-zorg#2LG) (lg-2)
  * [LG Wonen met begeleiding en verzorging](http://purl.org/ozo/onz-zorg#4LG) (lg-4)
  * [LG Wonen met begeleiding en intensieve verzorging](http://purl.org/ozo/onz-zorg#5LG) (lg-5)
  * [LG Wonen met intensieve begeleiding en intensieve verzorging](http://purl.org/ozo/onz-zorg#6LG) (lg-6)
  * [LG Wonen met zeer intensieve begeleiding en zeer intensieve verzorging](http://purl.org/ozo/onz-zorg#7LG) (lg-7)
* **Opmerking:** De benamingen van de zorgprofielen zijn overgenomen uit de modelgegevensset. Tussen haakjes de ingeburgerde codes, die geen formeel ondereel uitmaken van de naam van de zorgprofielen.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer  alle _Zorgaanbod bij combinatie van leveringsvormen DTV_ (kolommen)
2. Bepaal per vestiging (rijen) welk zorgaanbod bij _combinatie van leveringsvormen DTV_ beschikbaar is. Rapporteer als het betreffende zorgaanbod beschikbaar is 'ja', en anders 'nee'.
3. Neem alleen vestigingen op in de tabel waarvoor bij minimaal één zorgprofiel 'ja' gerapporteerd wordt. Sorteer op alfabet.

Meetmoment: dd-mm-jjjj

| Organisatieonderdeel | Zorgprofiel 1      | Zorgprofiel 2    | ...    | Zorgprofiel n    |
| ---                  | :---:              | :---:            | :---:  | :---:            |
| Vestiging 1          | stap 2             | stap 2           | stap 2 | stap 2           |
| Vestiging 2          | stap 2             | stap 2           | stap 2 | stap 2           |
| ...                  |                    |                  |        |                  |
| Vestiging m          | stap 2             | stap 2           | stap 2 | stap 2           |

## Ter bespreking

| No. | Soort | Omschrijving | Bron / Link |
|-----|-------|--------------|-------------|
| 1   | Opmerking |          |             |
