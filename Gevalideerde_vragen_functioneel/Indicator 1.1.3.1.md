---
title = "1.1.3.1. In welke woonplaats(en) kan deze organisatie casemanagement dementie leveren bij Modulair Pakket Thuis (MPT)?"
---

**Status:** afstemmen ontologie, zonder berekening

# DEZE FUNCTIONELE BESCHRIJVING IS IN ONTWIKKELING

**Opmerking:** Om de dialoog te voeden staan ook vragen en discussie-onderwerpen in dit concept. Die worden beslecht en verwijderd voordat dit een als functionele beschrijving voor deze indidactor kan gelden.

## Definitie

**Definitie:** De beschikbaarheid op meetmoment van casemanagement dementie leveringsvorm Modulair Pakket Thuis (MPT).

## Toelichting

De informatievraag betreft het zorgaanbod bij leveringsvorm MPT dat per woonplaats geleverd kan worden. Dit betreft de bereidheid (en en daarmee de geschiktheid) om op die vestiging het gerapporteerde zorgaanbod te leveren onafhankelijk van actuele of historische leveringen.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* Het beleidsmatige voornemen om in bepaalde woonplaats(en) bepaalde zorg in bepaalde leveringsvormen te (willen) leveren, en daarmee het aanbod op de markt wordt door de zorgorganisatie in tabelvorm gevoed (bronsysteem).
* **Opmerking:** Casemanagement dementie is volgens het zorginstituut in [Memo verduidelijking casemanagement](https://www.zorginstituutnederland.nl/binaries/zinl/documenten/standpunten/2018/03/22/verduidelijking-standpunt-casemanagement/Verduidelijking+casemanagement.pdf) voor hun doel onvoldoende gedefinieerd. Wellicht dat voor ons doel de definitie in [Zorgstandaard Dementie 2020](https://www.dementiezorgvoorelkaar.nl/wp-content/uploads/2022/01/zorgstandaard-dementie.pdf) op pagina 18 wel een voldoende basis is.
