---
title: 1.1.1. Welk zorgaanbod kan deze vestiging leveren bij leveringsvorm Verblijf?
weight: n.t.b.
---

## Indicator

**Definitie:** Op meetmoment beschikbaar zorgaanbod bij leveringsvorm Verblijf per vestiging.

## Toelichting

De informatievraag betreft het zorgaanbod bij leveringsvorm Verblijf dat per vestiging geleverd kan worden. Dit betreft de bereidheid (en en daarmee de geschiktheid) om op die vestiging het gerapporteerde zorgaanbod te leveren onafhankelijk van actuele of historische leveringen.

## Uitgangspunten

* Uitwerking binnen het kader van de Wlz.
* Het beleidsmatige voornemen om op of vanuit bepaalde vestigingen bepaalde zorg in bepaalde leveringsvormen te (willen) leveren, en daarmee het aanbod op de markt wordt door de zorgorganisatie in tabelvorm gevoed (bronsysteem). Dit zorgaanbod wordt bij leveringsvorm Verblijf uitgedrukt in Zorgprofielen.
* _Zorgaanbod bij leveringsvorm Verblijf_ is de beschikbaarheid van zorg benodigd t.b.v. onderstaande _zorgprofielen_ geleverd via de _leveringsvorm_ Verblijf.
  * [VV Beschut wonen met enige begeleiding (2015)](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_723961_22_1&bestand=Bijlage_1_bij_BR-REG-23122a_Overzicht_zorgprofielen_en_bijbehorende_zzp%27s.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-23122a+Overzicht+zorgprofielen+en+bijbehorende+zzp%27s.pdf) (vv-1)(site NZa)
  * [VV Beschut wonen met begeleiding en verzorging (2015)](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_723961_22_1&bestand=Bijlage_1_bij_BR-REG-23122a_Overzicht_zorgprofielen_en_bijbehorende_zzp%27s.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-23122a+Overzicht+zorgprofielen+en+bijbehorende+zzp%27s.pdf) (vv-2)(site NZa)
  * [VV Beschut wonen met begeleiding en intensieve verzorging (2015)](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_723961_22_1&bestand=Bijlage_1_bij_BR-REG-23122a_Overzicht_zorgprofielen_en_bijbehorende_zzp%27s.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-23122a+Overzicht+zorgprofielen+en+bijbehorende+zzp%27s.pdf) (vv-3)(site NZa)
  * [VV Beschut wonen met intensieve begeleiding en uitgebreide verzorging](http://purl.org/ozo/onz-zorg#4VV) (vv-4)
  * [VV Beschermd wonen met intensieve dementiezorg](http://purl.org/ozo/onz-zorg#5VV) (vv-5)
  * [VV Beschermd wonen met intensieve verzorging en verpleging](http://purl.org/ozo/onz-zorg#6VV) (vv-6)
  * [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding](http://purl.org/ozo/onz-zorg#7VV) (vv-7)
  * [VV Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging en verpleging](http://purl.org/ozo/onz-zorg#8VV) (vv-8)
  * [VV Beschermd verblijf met intensieve palliatief-terminale zorg](http://purl.org/ozo/onz-zorg#10VV) (vv-10)
  * [VV Herstelgerichte behandeling met verpleging en verzorging](http://purl.org/ozo/onz-zorg#9BVV) (vv-9b)
  * [LG Wonen met enige begeleiding en enige verzorging (2015)](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_723961_22_1&bestand=Bijlage_1_bij_BR-REG-23122a_Overzicht_zorgprofielen_en_bijbehorende_zzp%27s.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-23122a+Overzicht+zorgprofielen+en+bijbehorende+zzp%27s.pdf) (lg-1)(site NZa)
  * [LG Wonen met begeleiding en enige verzorging](http://purl.org/ozo/onz-zorg#2LG) (lg-2)
  * [LG Wonen met enige begeleiding en verzorging (2015)](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_723961_22_1&bestand=Bijlage_1_bij_BR-REG-23122a_Overzicht_zorgprofielen_en_bijbehorende_zzp%27s.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-23122a+Overzicht+zorgprofielen+en+bijbehorende+zzp%27s.pdf) (lg-3)(site NZa)
  * [LG Wonen met begeleiding en verzorging](http://purl.org/ozo/onz-zorg#4LG) (lg-4)
  * [LG Wonen met begeleiding en intensieve verzorging](http://purl.org/ozo/onz-zorg#5LG) (lg-5)
  * [LG Wonen met intensieve begeleiding en intensieve verzorging](http://purl.org/ozo/onz-zorg#6LG) (lg-6)
  * [LG Wonen met zeer intensieve begeleiding en zeer intensieve verzorging](http://purl.org/ozo/onz-zorg#7LG) (lg-7)
* **Opmerking:** Tussen haakjes de ingeburgerde codes, die geen formeel ondereel uitmaken van de naam van de zorgprofielen.
* **Opmerking:** De benamingen van de zorgprofielen zijn overgenomen uit de modelgegevensset. Daar waar ze (nog) ontbreken zijn ze overgenomen uit [Bijlage 1 bij Beleidsregel prestatiebeschrijving en tarieven zorgzwaartepakketten en volledig pakket thuis 2023](https://puc.overheid.nl/PUC/Handlers/DownloadBijlage.ashx?pucid=PUC_723961_22_1&bestand=Bijlage_1_bij_BR-REG-23122a_Overzicht_zorgprofielen_en_bijbehorende_zzp%27s.pdf&bestandsnaam=Bijlage+1+bij+BR-REG-23122a+Overzicht+zorgprofielen+en+bijbehorende+zzp%27s.pdf). Tussen haakjes de ingeburgerde codes, die geen formeel ondereel uitmaken van de naam van de zorgprofielen.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer  alle _zorgaanbod bij leveringsvorm Verblijf_ (kolommen)
2. Bepaal per vestiging (rijen) welk zorgaanbod bij _leveringsvorm Verblijf_ beschikbaar is. Rapporteer als het betreffende zorgaanbod beschikbaar is 'ja', en anders 'nee'.
3. Neem alleen vestigingen op in de tabel waarvoor bij minimaal één zorgprofiel 'ja' gerapporteerd wordt. Sorteer op alfabet.

Meetmoment: dd-mm-jjjj

| Organisatieonderdeel | Zorgprofiel 1      | Zorgprofiel 2    | ...    | Zorgprofiel n    |
| ---                  | :---:              | :---:            | :---:  | :---:            |
| Vestiging 1          | stap 2             | stap 2           | stap 2 | stap 2           |
| Vestiging 2          | stap 2             | stap 2           | stap 2 | stap 2           |
| ...                  |                    |                  |        |                  |
| Vestiging m          | stap 2             | stap 2           | stap 2 | stap 2           |

## Ter bespreking

| No. | Soort | Omschrijving | Bron / Link |
|-----|-------|--------------|-------------|
| 1   | Opmerking | De zorgprofielen vv-1, vv-2, vv-3, lg-1 en lg-3 zijn onderdeel van een overgangsregeling, als deze opgenomen zijn in de ontologie kunnen de verwijzingen hierboven aangepast worden. | |
