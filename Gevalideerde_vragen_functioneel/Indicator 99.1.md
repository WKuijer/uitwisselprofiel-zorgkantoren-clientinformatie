---
title: 99.1. In welke mate wordt rekening gehouden met de wensen rondom levenseinde door deze vestiging?
---

# DEZE FUNCTIONELE BESCHRIJVING IS IN ONTWIKKELING

## Indicator

**Definitie:** Percentage cliënten op de vestiging waarbij beleidsafspraken rondom het levenseinde zijn vastgelegd in het zorgdossier.

**Teller:** Aantal cliënten waarbij beleidsafspraken zijn vastgelegd in het zorgdossier.

**Noemer:** Totaal aantal cliënten op de vestiging.

## Toelichting

De indicator betreft per vestiging het totaal aantal cliënten waarbij beleidsafspraken rondom het levenseinde zijn vastgelegd in het zorgdossier ten op zichten van het totaal aantal cliënten per vestiging. Het betreft cliënten met een Wlz-indicatie met een zorgprofiel VV4 t/m VV10. De indicator wordt per vestiging berekend. De meetperiode is 1 januari 2022 t/m 31 december 2022. Cliënten die gedurende de meetperiode van vestiging wisselen worden ingedeeld bij de meest recente vestiging (binnen de meetperiode).

## Uitgangspunten

* Alle cliënten met een Wlz-indicatie VV4 tot en met VV10 binnen de meetperiode worden geïncludeerd.
* In geval van meerdere afspraken bij een cliënt telt deze cliënt één keer mee.
* Cliënten worden ingedeeld op de meeste recente vestiging waar de cliënt (o.b.v. de indicatie) zorg ontving binnen de meetperiode.
* De datum van de beleidsafspraak moet voor de einddatum van de meetperiode liggen, maar kan voor de startdatum van de meetperiode liggen.

## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10 binnen de meetperiode.
2. Bepaal o.b.v. stap 1 per client de meest recente vestiging binnen de meetperiode.  
3. Bepaal per cliënt of er minimaal één beleidsafspraak rond het levenseinde is geregistreerd.
4. Bereken per (meest recente) vestiging het aantal cliënten.
5. Bereken per (meest recente) vestiging het aantal cliënten bij wie minimaal een afspraak rond het levenseinde is geregistreerd.
6. Bereken de indicator door per vestiging het resultaat uit stap ‘5’ te delen door het resultaat uit stap ‘4’ en vermenigvuldig met 100%.
