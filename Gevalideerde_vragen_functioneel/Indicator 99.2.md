---
title: 99.2. In welke mate wordt rekening gehouden met mijn voedselvoorkeuren door deze vestiging?
---

# DEZE FUNCTIONELE BESCHRIJVING IS IN ONTWIKKELING

## Indicator

**Definitie:** Percentage cliënten op de vestiging waarbij voedselvoorkeuren in de afgelopen zes maanden zijn besproken en vastgelegd in het zorgdossier.

**Teller:** Aantal cliënten waarmee voedselvoorkeuren in de afgelopen zes maanden zijn besproken en waarbij de afspraken zijn vastgelegd in het zorgdossier.

**Noemer:** Totaal aantal cliënten op de vestiging.

## Toelichting

De indicator betreft per vestiging het totaal aantal cliënten waarbij beleidsafspraken over voedselvoorkeuren zijn vastgelegd in het zorgdossier ten op zichten van het totaal aantal cliënten per vestiging. Het betreft cliënten met een Wlz-indicatie met een zorgprofiel VV4 t/m VV10. De indicator wordt per vestiging berekend. De meetperiode is 1 juli 2022 t/m 31 december 2022. Cliënten die gedurende de meetperiode van vestiging wisselen worden ingedeeld bij de meest recente vestiging (binnen de meetperiode).

## Uitgangspunten

* Alle cliënten met een Wlz-indicatie VV4 tot en met VV10 binnen de meetperiode worden geïncludeerd.
* In geval van meerdere afspraken bij een cliënt telt deze cliënt één keer mee.
* Cliënten worden ingedeeld op de meeste recente vestiging waar de cliënt (o.b.v. de indicatie) zorg ontving binnen de meetperiode.

## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10 binnen de meetperiode.
2. Bepaal o.b.v. stap 1 per client de meest recente vestiging binnen de meetperiode.
3. Bepaal per cliënt of er minimaal één beleidsafspraak over voedselvoorkeuren is geregistreerd binnen de meetperiode.
4. Bereken per (meest recente) vestiging het aantal cliënten.
5. Bereken per (meest recente) vestiging het aantal cliënten bij wie minimaal één afspraak over voedselvoorkeuren is geregistreerd.
6. Bereken de indicator door per vestiging het resultaat uit stap ‘5’ te delen door het resultaat uit stap ‘4’ en vermenigvuldig met 100%.
